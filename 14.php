<!DOCTYPE html>

<html>
  <head>
    <meta charset="UTF-8">
    <title>TallerPHP</title>
  </head>
  <body>
    <?php
    $pets = array
        (
        array("Dog", "Big", "5"),
        array("Cat", "Small", "10"),
        array("Pig", "Big", "20"),
    );
    echo "My pet is a " . $pets[0][0] . " is " . $pets[0][1] . " and he is " . $pets[0][2] . " years old.";
    echo "My pet is a " . $pets[1][0] . " is " . $pets[1][1] . " and he is " . $pets[1][2] . " years old.";
    echo "My pet is a " . $pets[2][0] . " is " . $pets[2][1] . " and he is " . $pets[2][2] . " years old.";
    ?>
  </body>
</html>

