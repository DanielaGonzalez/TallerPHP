<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>TallerPHP</title>
  </head>
  <body>
    <?php
    //FunciÃ³n Ordernar (Sort)
    $frutas = array("limÃ³n", "naranja", "banana", "albaricoque");
    sort($frutas);
    foreach ($frutas as $clave => $valor) {
      echo "frutas[" . $clave . "] = " . $valor . "\n";
    }

    //FunciÃ³n rsort
    $fruits = array("limÃ³n", "naranja", "plÃ¡tano", "manzana");
    rsort($fruits);
    foreach ($fruits as $key => $val) {
      echo "$key = $val\n";
    }

    //FunciÃ³n asort
    $fruits = array("d" => "lemon", "a" => "orange", "b" => "banana", "c" => "apple");
    asort($fruits);
    foreach ($fruits as $key => $val) {
      echo "$key = $val\n";
    }

    //FunciÃ³n ksort
    $fruits = array("d" => "lemon", "a" => "orange", "b" => "banana", "c" => "apple");
    ksort($fruits);
    foreach ($fruits as $key => $val) {
      echo "$key = $val\n";
    }

    //FunciÃ³n arsort
    $fruits = array("d" => "lemon", "a" => "orange", "b" => "banana", "c" => "apple");
    arsort($fruits);
    foreach ($fruits as $key => $val) {
      echo "$key = $val\n";
    }

    //FunciÃ³n krsort
    $fruits = array("d" => "lemon", "a" => "orange", "b" => "banana", "c" => "apple");
    krsort($fruits);
    foreach ($fruits as $key => $val) {
      echo "$key = $val\n";
    }
    ?>
  </body>
</html>


