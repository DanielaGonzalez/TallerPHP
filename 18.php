<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>TallerPHP</title>
  </head>
  <body>
    <?php
    //if
    $t1 = date("H");

    if ($t1 < "20") {
      echo "Have a good day!";
    }

    //if...else
    $t2 = date("H");

    if ($t2 < "20") {
      echo "Have a good day!";
    } else {
      echo "Have a good night!";
    }

    //if...else if... else

    $t3 = date("H");

    if ($t3 < "10") {
      echo "Have a good morning!";
    } elseif ($t3 < "20") {
      echo "Have a good day!";
    } else {
      echo "Have a good night!";
    }

    //while

    $est1 = 1;

    while ($est1 <= 5) {
      echo "The number is: $est1 <br>";
      $est1++;
    }

    //do...while

    $est2 = 1;

    do {
      echo "The number is: $est2 <br>";
      $est2++;
    } while ($est2 <= 5);

    //for
    for ($t4 = 0; $t4 <= 10; $t4++) {
      echo "The number is: $x <br>";
    }

    //foreach
    $colors = array("red", "green", "blue", "yellow");

    foreach ($colors as $value) {
      echo "$value <br>";
    }

    // Switch...break
    $favcolor = "red";

    switch ($favcolor) {
      case "red":
        echo "Your favorite color is red!";
        break;
      case "blue":
        echo "Your favorite color is blue!";
        break;
      case "green":
        echo "Your favorite color is green!";
        break;
      default:
        echo "Your favorite color is neither red, blue, nor green!";
    }

    //continue

    while (list($clave, $valor) = each($arr)) {
      if (!($clave % 2)) { // saltar los miembros pares
        continue;
      }
      hacer_algo($valor);
    }

    $i = 0;
    while ($i++ < 5) {
      echo "Exterior<br />\n";
      while (1) {
        echo "Medio<br />\n";
        while (1) {
          echo "Interior<br />\n";
          continue 3;
        }
        echo "Esto nunca se imprimirÃ¡.<br />\n";
      }
      echo "Ni esto tampoco.<br />\n";
    }

    //return
    $b = 1;
    echo "b";
    return;

    //require

    require('somefile.php');
    require 'somefile.php';

    //include
    //vars.php
    $color = 'verde';
    $fruta = 'manzana';

    //test.php
    echo "Una $fruta $color"; // Una

    include 'vars.php';

    echo "Una $fruta $color"; // Una manzana verde
    //require_once

    define('__ROOT__', dirname(dirname(__FILE__)));
    require_once(__ROOT__ . '/config.php');

    require_once('/var/www/public_html/config.php');

    //include_once
    include_once "a.php"; // esto incluirÃ¡ a.php
    ?>
  </body>
</html>

