<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>TallerPHP</title>
  </head>
  <body>
    <?php
    echo "Hello World!";
    // This is a single-line comment
    echo "I am learning PHP";
    /* This is a multi-line comment block
     * that spans over
     * multiple lines
     */
    ?>
  </body>
</html>
