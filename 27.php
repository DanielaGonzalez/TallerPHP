<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>TallerPHP</title>
  </head>
  <body>
    <?php

// --CONSTRUCT
    class BaseClass {

      function __construct() {
        print "En el constructor BaseClass\n";
      }

    }

    class SubClass extends BaseClass {

      function __construct() {
        parent::__construct();
        print "En el constructor SubClass\n";
      }

    }

    class OtherSubClass extends BaseClass {
      // heredando el constructor BaseClass
    }

// En el constructor BaseClass
    $obj = new BaseClass();

// En el constructor BaseClass
// En el constructor SubClass
    $obj = new SubClass();

// En el constructor BaseClass
    $obj = new OtherSubClass();

//DESTRUCT
    class MyDestructableClass {

      function __construct() {
        print "En el constructor\n";
        $this->name = "MyDestructableClass";
      }

      function __destruct() {
        print "Destruyendo " . $this->name . "\n";
      }

    }

    $obj = new MyDestructableClass();

// __TOSTRING
    class TestClass {

      public $foo;

      public function __construct($foo) {
        $this->foo = $foo;
      }

      public function __toString() {
        return $this->foo;
      }

    }

    $class = new TestClass('Hola Mundo');
    echo $class;
    ?>
  </body>
</html>

