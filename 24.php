<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>TallerPHP</title>
  </head>
  <body>
    <?php

    class ClaseSencilla {

      // Declaración de una propiedad
      public $var = 'un valor predeterminado';

      // Declaración de un método
      public function mostrarVar() {
        echo $this->var;
      }

    }
    ?>
  </body>
</html>

