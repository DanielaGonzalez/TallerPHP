<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>TallerPHP</title>
  </head>
  <body>
    <?php
    //$_SERVER
    echo $_SERVER['PHP_SELF'];
    echo "<br>";
    echo $_SERVER['SERVER_NAME'];
    echo "<br>";
    echo $_SERVER['HTTP_HOST'];
    echo "<br>";
    echo $_SERVER['HTTP_REFERER'];
    echo "<br>";
    echo $_SERVER['HTTP_USER_AGENT'];
    echo "<br>";
    echo $_SERVER['SCRIPT_NAME'];

//$_REQUEST
    <form method = "post" action = "<?php echo $_SERVER['PHP_SELF'];?>">
    Name: <input type = "text" name = "fname">
    <input type = "submit">
    </form>

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // collect value of input field
    $name = $_REQUEST['fname'];
    if (empty($name)) {
    echo "Name is empty";
    } else {
    echo $name;
    }
    }

    //$_POST
    <form method = "post" action = "<?php echo $_SERVER['PHP_SELF'];?>">
    Name: <input type = "text" name = "fname">
    <input type = "submit">
    </form>

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // collect value of input field
    $name = $_POST['fname'];
    if (empty($name)) {
    echo "Name is empty";
    } else {
    echo $name;
    }
    }

    //$_GET
    echo "Study " . $_GET['subject'] . " at " . $_GET['web'];

    //$_COOKIE
    echo '¡Hello ' . htmlspecialchars($_COOKIE["name"]) . '!';
    ?>
  </body>
</html>

