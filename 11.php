<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>TallerPHP</title>
  </head>
  <body>
    <?php
    //option 1
    $cars = array("Toyota", "BMW", "Chevrolet");
    //option 2
    $cars [0] = "Toyota";
    $cars [1] = "BMW";
    $cars [2] = "Chevrolet";

    echo "I like " . $cars [0] . " , " . $cars[1] . " and " . $cars[2];
    ?>
  </body>
</html>