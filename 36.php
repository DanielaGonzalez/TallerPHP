<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>TallerPHP</title>
  </head>
  <body>
    <?php
    //Ejemplo1
    error_reporting(E_ALL);
    ini_set('display_errors', '1');

//Ejemplo 2
// Motrar todos los errores de PHP
    error_reporting(-1);

// No mostrar los errores de PHP
    error_reporting(0);

// Motrar todos los errores de PHP
    error_reporting(E_ALL);

// Motrar todos los errores de PHP
    ini_set('error_reporting', E_ALL);
    ?>
  </body>
</html>
