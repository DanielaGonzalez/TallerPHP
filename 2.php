<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title></title>
  </head>
  <body>
    <?php
    //String (Cadena de Texto)
    $name = "dani";
    //Integer (Entero)
    $phone = 3127591372;
    //Float(Flotante)
    $x = 4.123;
    //Boolean (Booleano)
    $bool = true;
    //Array (Arreglo)
    $names = array("Camilo", "Andres", "Sara");

    //Object (Objeto)

    class dani {

      function hacer_algo() {
        echo "Haciendo algo.";
      }

    }

    $bar = new dani;
    $bar->hacer_algo();

    //NULL (Nulo)
    $age = NULL;

    //Iterable
    function dani(iterable $iterable) {
      foreach ($iterable as $valor) {
        // ...
      }
    }
    ?>
  </body>
</html>

